import { getRandomFrom, getRandomIntInclusive, getRandomColor } from './init.js';
import { devMode, assetPath, assetSuffix, platformTest, listOfRunStates, listOfImages, listOfBlemdModeLists } from './settings.js';

function Thing(tag) {
    'use strict';
    if (this instanceof Thing == false) {
        if (devMode === true) {console.log(`!!! - Thing called w/out "new" keyword - !!!!`);}
        return new Thing();
    } else {
        this.tag = tag;
        this.buttonSuppressor = false;
        this.buttonSuppressorDuration = 500;
        this.paused = false;
        this.imagesReady = `false`;
        // this.frameLengthDirection = true;
        this.setupInit.bind(this)();
    }

}
Thing.prototype = {
    constructor: Thing,
    sayHello: function() {
        'use strict';
        console.log(`Thing${this.tag} says: 'Hello!'`);
    },
    setupInit: function () {
        'use strict';

        this.thingContainer = document.createElement(`div`);
        this.thingContainer.id = `thingContainer${this.tag}`;
        this.thingContainer.className = `thingContainer`;
        document.getElementsByClassName(`windowContainer`)[0].appendChild(this.thingContainer);

        this.thingView = document.createElement(`div`);
        this.thingView.id = `thingView${this.tag}`;
        this.thingView.className = `thingView`;
        this.thingContainer.appendChild(this.thingView);

        this.resBadge = document.createElement(`p`);
        this.resBadgeLoadingTextList = [`.`,`..`,`...`,`....`,`.....`];
        this.resBadge.count = -1;
        this.resBadge.ready = false;
        this.resBadge.id = `resBadge`;
        this.resBadge.innerText = `.`;
        document.querySelector(`.windowContainer`).appendChild(this.resBadge);
        this.resBadgeLoadingAnimation.call(this);
        // https://stackoverflow.com/questions/45300903/canvas-drawing-takes-a-lot-of-time-on-safari-but-not-on-chrome-or-ff

        this.textBox = document.createElement(`div`);
        this.textBox.id = `textBox`;
        this.textBox.className = `textBox`;
        this.thingContainer.appendChild(this.textBox);

        this.setupUXUI.call(this);

        this.setupImages.call(this);
        // this.setupLoadingSpinner.bind(this)();
    },
    resBadgeLoadingAnimation: function() {
        this.resBadge.count++;
        this.resBadge.innerText = this.resBadgeLoadingTextList[this.resBadge.count%5];
        if (this.resBadge.ready != true) {
            setTimeout(this.resBadgeLoadingAnimation.bind(this), 200);
        } else {
            if (assetPath.includes(`loRes`)) {
                this.resBadge.innerText = `( low resolution images loaded - ${this.loadTimerEnd.getTime() - window.loadTimerStart.getTime()}ms )`;
            } else if (assetPath.includes(`hiRes`)) {
                this.resBadge.innerText = `( high resolution images loaded - ${this.loadTimerEnd.getTime() - window.loadTimerStart.getTime()}ms )`;
            } else {
                this.resBadge.innerText = `( standard images loaded - ${this.loadTimerEnd.getTime() - window.loadTimerStart.getTime()}ms )`;
            }
        }
    },
    setupUXUI: function() {
        'use strict';
        this.clickHereTextA = document.createElement(`h1`);
        this.clickHereTextA.style.cursor = `pointer`;
        this.clickHereTextA.innerHTML = `full version<br/><span style='font-size: 2.5vmin;'>| dynamic image sequences |<br/>| up to 20 frames/sec |<br/><span class='warning'>| contains flashing lights/images |</span></span>`;
        this.textBox.appendChild(this.clickHereTextA);

        this.clickHereTextA.addEventListener(`click`, () => {
            this.frameLengthInit = 1000;
            this.frameLengthTarget = 50;
            this.clearFirst = true;
            this.runBasicVersion = false;
            this.frameLength = this.frameLengthInit;
            this.layerPercentages = [100, 50, 25];
            this.blendModeListSelections = [1, 4, 3];
            this.currentState = listOfRunStates[0];
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = false;
            this.currentFillColor = `transparent`;
            if (platformTest === false) {
                this.loSpecConfirmation.call(this);
            } else {
                this.setupLoadingSpinner.call(this);
            }
        }, {once: true});

        this.clickHereTextB = document.createElement(`h1`);
        this.clickHereTextB.style.cursor = `pointer`;
        this.clickHereTextB.style.alignSelf = `end`;
        this.clickHereTextB.innerHTML = `basic version<br/><span style='font-size: 2.5vmin'>| simple slide show |<br/>| approx. 1 frame/sec |</span>`;
        this.textBox.appendChild(this.clickHereTextB);

        this.clickHereTextB.addEventListener(`click`, () => {
            this.frameLength = 1000;
            this.runBasicVersion = true;
            this.layerPercentages = [100, 50, 25];
            this.blendModeListSelections = [1, 4, 3];
            this.currentFilter = `none`;
            this.setupLoadingSpinner.call(this);
        }, {once: true});
    },
    setupImages: function() {
        'use strict';
        if (devMode === true) {console.log(`loading images`);}
        this.listOfImgPromises = [];
        this.listOfImgs = [];
        listOfImages.map((x, index) => {
            this[`imgPromise${(`000` + index).slice(-3)}`] = () => {
                return new Promise((resolve, reject) => {
                    this[`image${(`000` + index).slice(-3)}`] = new Image();
                    this[`image${(`000` + index).slice(-3)}`].onload = () => {
                        this.listOfImgs.push(this[`image${(`000` + index).slice(-3)}`]);
                        resolve([x, index]);
                    };
                    this[`image${(`000` + index).slice(-3)}`].onerror = () => {
                        reject(console.log(`!!! - ERROR - !!!`));
                    };
                    // setTimeout(() => {
                    this[`image${(`000` + index).slice(-3)}`].src = `${assetPath}${x}${assetSuffix}.png`;
                    // }, getRandomIntInclusive(15000,15000));
                });
            };
            this.listOfImgPromises.push(this[`imgPromise${(`000` + index).slice(-3)}`]());
        });

        Promise.all(this.listOfImgPromises).then((args) => {
            // this.main.bind(this)();
            this.imagesReady = true;
            this.loadTimerEnd = new Date();
            setTimeout(() => {
                this.resBadge.ready = true;
            }, 500);
            if (devMode === true) {console.log(`images loaded`);}

        });
    },
    loSpecConfirmation: function() {
        'use strict';
        this.clickHereTextA.parentElement.removeChild(this.clickHereTextA);
        this.clickHereTextB.parentElement.removeChild(this.clickHereTextB);
        this.clickHereTextA = null;
        this.clickHereTextB = null;

        this.textBox.style.height = `65%`;
        this.textBox.style.width = `75%`;

        this.loSpecConfirmationText = document.createElement(`p`);
        this.loSpecConfirmationText.style.alignSelf = `start`;
        this.loSpecConfirmationText.innerHTML = `it looks like you may not be running firefox or chrome -- or you may not be using a desktop computer.  because of this, performance may be uneven and the intended frame rates may not be achieved.`;
        this.textBox.appendChild(this.loSpecConfirmationText);

        this.loSpecConfirmationOption_continueAnyway = document.createElement(`p`);
        this.loSpecConfirmationOption_continueAnyway.className = `pLink`;
        this.loSpecConfirmationOption_continueAnyway.style.cursor = `pointer`;
        this.loSpecConfirmationOption_continueAnyway.style.alignSelf = `center`;
        this.loSpecConfirmationOption_continueAnyway.innerHTML = `<span style='color:blue'>[ click here ]</span><br/>if you would like to continue anyway<br/>(intended frame rates may not be achieved)`;
        this.textBox.appendChild(this.loSpecConfirmationOption_continueAnyway);

        this.loSpecConfirmationOption_continueAnyway.addEventListener(`click`, () => {
            this.backButton.parentElement.removeChild(this.backButton);
            this.backButton = null;
            this.setupLoadingSpinner.call(this);
        }, {once: true});

        this.loSpecConfirmationOption_videoClip = document.createElement(`p`);
        this.loSpecConfirmationOption_videoClip.className = `pLink`;
        this.loSpecConfirmationOption_videoClip.style.cursor = `pointer`;
        this.loSpecConfirmationOption_videoClip.style.alignSelf = `end`;
        this.loSpecConfirmationOption_videoClip.innerHTML = `<span style='color:blue'>[ click here ]</span><br/>if you would like to play a short video clip of the presentation instead`;
        this.textBox.appendChild(this.loSpecConfirmationOption_videoClip);

        this.loSpecConfirmationOption_videoClip.addEventListener(`click`, () => {
            window.open(`https://concretesunrise.net/18/NPDU1/le_altClip/`, `_self`);
        }, {once: true});

        this.backButton = document.createElement(`p`);
        this.backButton.className = `pLink`;
        this.backButton.style.cursor = `pointer`;
        this.backButton.style.alignSelf = `start`;
        this.backButton.style.justifySelf = `start`;
        this.backButton.style.margin = `1.5vmin 0 0 2vmin`;
        // this.backButton.style.fontSize = `1.5vmin`;
        this.backButton.innerHTML = `<=`;
        this.thingContainer.appendChild(this.backButton);

        this.backButton.addEventListener(`click`, () => {
            this.loSpecConfirmationText.parentElement.removeChild(this.loSpecConfirmationText);
            this.loSpecConfirmationOption_continueAnyway.parentElement.removeChild(this.loSpecConfirmationOption_continueAnyway);
            this.loSpecConfirmationOption_videoClip.parentElement.removeChild(this.loSpecConfirmationOption_videoClip);
            this.backButton.parentElement.removeChild(this.backButton);

            this.loSpecConfirmationText = null;
            this.loSpecConfirmationOption_continueAnyway = null;
            this.loSpecConfirmationOption_videoClip = null;
            this.backButton = null;

            this.setupUXUI.call(this);

        }, {once: true});

    },
    setupLoadingSpinner: function () {
        'use strict';

        if (platformTest == false && this.runBasicVersion == false) {

            this.platformConflictText = document.createElement(`p`);
            this.platformConflictText.id = `platformConflictText`;
            this.platformConflictText.innerText = `*** for best performance, please use chrome/firefox on a desktop computer. ***`;
            document.querySelector(`.windowContainer`).appendChild(this.platformConflictText);
        }

        this.textBox.parentElement.removeChild(this.textBox);
        this.textBox = null;

        this.loadSpinner = document.createElement(`div`);
        this.loadSpinner.id = `loadSpinner`;
        this.loadSpinner.className = `loadingSpinner`;
        this.thingView.appendChild(this.loadSpinner);

        this.holdForImages.call(this);
    },
    holdForImages: function() {
        'use strict';
        this.imagesReady === true ? (() => {
            this.main.call(this);
        })() : setTimeout(() => {
            this.holdForImages.call(this);
        }, 100);
    },
    main: function() {
        'use strict';

        this.thingView.addEventListener(`click`, () => {
            this.playPause.call(this);
        });

        this.menuButton = document.createElement(`p`);
        this.menuButton.id = `menuButton`;
        this.menuButton.innerText = `|||`;
        document.querySelector(`.windowContainer`).appendChild(this.menuButton);

        this.menuButton.addEventListener(`click`, () => {
            this.buttonSuppressor === false ? (() => {
                this.buttonSuppressor = true;
                setTimeout(() => {
                    this.buttonSuppressor = false;
                }, 500);
                this.vidLinkBox.style.display === `grid` ? (() => {
                    // this.vidLinkBox.style.display = `none`;
                    this.vidLinkBox.addEventListener(`transitionend`, () => {
                        this.vidLinkBox.style.display = `none`;
                    }, {once: true});
                    this.vidLinkBox.style.opacity = `0`;
                })() : (() => {
                    this.vidLinkBox.style.display = `grid`;
                    setTimeout(() => {
                        this.vidLinkBox.style.opacity = `1`;
                    }, 0);
                })();
            })() : (() => {
                console.log(`!!! - BUTTON SUPPRESSED - !!!`);
            })();
        });

        this.vidLinkBox = document.createElement(`div`);
        this.vidLinkBox.id = `vidLinkBox`;
        document.querySelector(`.windowContainer`).appendChild(this.vidLinkBox);

        this.vidLink = document.createElement(`a`);
        this.vidLink.title = `vid`;
        this.vidLink.id = `vidLink`;
        // this.vidLink.className= `pLink`;
        this.vidLink.href = `https://concretesunrise.net/18/NPDU1/le_altClip/`;
        this.vidLink.target = `_self`;
        this.vidLink.innerHTML = `<p class='pLink' style='padding-bottom: 0.25vmin'>open as video</p>`;
        this.vidLinkBox.appendChild(this.vidLink);

        this.viewCanvas = document.createElement(`canvas`);
        this.viewCanvas.id = `viewCanvas`;
        this.viewCanvas.className = `viewCanvas`;
        this.thingView.appendChild(this.viewCanvas);
        this.viewCtx = this.viewCanvas.getContext(`2d`);

        if (devMode != false) {
            this.downloadCanvas = document.createElement(`canvas`);
            this.downloadCanvas.id = `viewCanvas`;
            this.downloadCanvas.className = `viewCanvas`;
            this.downloadCanvas.style.zIndex = `-100`;
            this.thingView.appendChild(this.downloadCanvas);
            this.downloadCtx = this.downloadCanvas.getContext(`2d`);

            this.downloadImageButton = document.createElement(`button`);
            this.downloadImageButton.id = `downloadImageButton`;
            this.downloadImageButton.className = `downloadImageButton`;
            document.body.appendChild(this.downloadImageButton);
    
            this.downloadImageButtonText = document.createElement(`p`);
            this.downloadImageButtonText.id = `downloadImageButtonText`;
            this.downloadImageButtonText.className = `downloadImageButtonText`;
            this.downloadImageButtonText.innerHTML = `dwnld`;
            this.downloadImageButton.appendChild(this.downloadImageButtonText);
    
            this.downloadImageButton.addEventListener(`click`, ()=> {
                this.buttonRouter(this.downloadImage);
            }, false);
        }

        setTimeout(() => {
            this.drawCanvas.bind(this)(); 
        }, 0);
        this.viewCanvas.addEventListener(`transitionend`, () => {
            this.loadSpinner.style.display = `none`;
        }, {once: true});
        setTimeout(() => {
            this.viewCanvas.style.opacity = `1`;
            window.resizeCanvas();
            // this.thingView.style.zIndex = `10`;
            // this.manageFrameLength.bind(this)();
            this.manageDelayRange= [4000, 8000];
            if (this.runBasicVersion === false) {
                setTimeout(() => {
                    this.manageVariables.call(this);
                }, 5000);
            }
        }, 2000);
    },
    buttonRouter: function(cFunc) {
        "use strict";
        this.buttonSuppressor === false ?
            (() => {
                this.buttonSuppressor = true;
                cFunc.call(this);
            })() : (() => {
                this.buttonSuppressor = true;
                console.log(`!!! - BUTTON SUPPRESSED - !!!`);
            })();
        setTimeout(() => {
            this.buttonSuppressor = false;
        }, this.buttonSuppressorDuration);
    },
    playPause: function() {
        'use strict';
        this.buttonSuppressor === false ? (() => {
            this.paused === true ? (() => {
                this.paused = false;
                this.drawCanvas.call(this);
            })() : (() => {
                this.paused = true;
                clearTimeout(this.drawCanvasTimeout);
            })();
            if (devMode === true) {console.log(`playPause: ${this.paused}`);}
        })() : (() => {
            console.log(`!!! - BUTTONS SUPPRESSED - !!!`);
        })();
        this.buttonSuppressor = true;
        setTimeout(() => {
            this.buttonSuppressor = false;
        }, this.buttonSuppressorDuration);
    },
    drawCanvas: function(onePlay) {
        'use strict';

        if (this.runBasicVersion === true || (this.runBasicVersion === false && this.clearFirst === true)) {
            this.viewCtx.clearRect(0,0, this.viewCanvas.clientWidth, this.viewCanvas.clientHeight);
        }

        if (this.runBasicVersion === false) {
            this.currentFillColor === `random` ? this.viewCtx.fillStyle = getRandomColor(`rgb`) : this.viewCtx.fillStyle = this.currentFillColor;
            this.viewCtx.fillRect(0, 0, this.viewCanvas.clientHeight, this.viewCanvas.clientWidth);

            this.viewCtx.globalCompositeOperation = getRandomFrom(listOfBlemdModeLists[this.blendModeListSelections[0]]);
        }
        this.paintImageOnCanvas.apply(this, [100, 250, 0]);

        if (/*this.runBasicVersion === false && */getRandomIntInclusive(1, 100) <= this.layerPercentages[1]) {
            this.viewCtx.globalCompositeOperation = getRandomFrom(listOfBlemdModeLists[this.blendModeListSelections[1]]);
            this.paintImageOnCanvas.apply(this, [100, 500, 1]);
        }

        if (/*this.runBasicVersion === false && */getRandomIntInclusive(1, 100) <= this.layerPercentages[2]) {
            this.viewCtx.globalCompositeOperation = getRandomFrom(listOfBlemdModeLists[this.blendModeListSelections[2]]);
            this.paintImageOnCanvas.apply(this, [100, 750, 2]);
        }

        // if (this.runBasicVersion === false) {
        //     if (this.frameLengthDirection === false && this.frameLength > 50) {
        //         this.frameLength -= 2;
        //     } else if (this.frameLengthDirection === false && this.frameLength <= 50) {
        //         this.frameLengthDirection = true;
        //         this.frameLength += 2;
        //     } else if (this.frameLengthDirection === true && this.frameLength < this.frameLengthInit) {
        //         this.frameLength += 2;
        //     } else if (this.frameLengthDirection === true && this.frameLength >= this.frameLengthInit) {
        //         this.frameLengthDirection = false;
        //         this.frameLength -=2;
        //     }
        //     console.log(`${this.frameLengthDirection} ${this.frameLength}`);
        // }

        // if (this.runBasicVersion === false && this.currentState === 0) {
        //     if (this.frameLength > this.frameLengthTarget) {
        //         this.frameLength = Math.round(this.frameLength * 0.95);
        //     } else if (this.frameLengthTarget <= 50) {
        //         this.frameLength= this.frameLengthTarget;
        //     }
        //     // console.log(`${this.frameLength}`);
        // }

        if (onePlay == undefined) {
            this.drawCanvasTimeout = setTimeout(() => {
                this.drawCanvas.call(this);
            }, this.frameLength);
        }

        // getRandomIntInclusive(1, 100) <= this.frameLength/25 ? this.frameLength = getRandomFrom([50, 100, 200, 250, 350, 500]) : this.frameLength = this.frameLength;
        // requestAnimationFrame(this.drawCanvas.bind(this));
    },
    paintImageOnCanvas: function(sizeLimitA, sizeLimitB, layer) {
        'use strict';

        this.viewCtx.save();

        if (this.runBasicVersion === false) {
            if (layer !== 0 && this.topBlur === true) {
                this.viewCtx.filter = this.currentFilter + ` blur(${getRandomIntInclusive(0,20)}px)`;
            } else {
                this.viewCtx.filter = this.currentFilter;
            }
        }


        let oversize = getRandomIntInclusive(sizeLimitA, sizeLimitB)/100;
        let imageXsize = oversize*this.viewCanvas.clientWidth;
        let imageYsize =oversize*this.viewCanvas.clientHeight;
        let imageXplacement = (getRandomIntInclusive(this.viewCanvas.clientWidth - imageXsize ,0));
        let imageYplacement = (getRandomIntInclusive(this.viewCanvas.clientHeight - imageYsize ,0));

        let imageFlippingStatus = getRandomFrom([0,1,2,3]);
        if (imageFlippingStatus === 1) {
            this.viewCtx.scale(-1, 1);
            this.viewCtx.translate(this.viewCanvas.clientWidth * -1, 0);
        } else if (imageFlippingStatus === 2) {
            this.viewCtx.scale(-1, -1);
            this.viewCtx.translate(this.viewCanvas.clientWidth * -1, this.viewCanvas.clientHeight * -1);
        } else if (imageFlippingStatus === 3) {
            this.viewCtx.scale(1, -1);
            this.viewCtx.translate(0, this.viewCanvas.clientHeight * -1);
        }

        let imageRotationStatus = getRandomFrom([0, 90, 180, 270]);
        if (imageRotationStatus === 0) {
            this.viewCtx.drawImage(getRandomFrom(this.listOfImgs), imageXplacement, imageYplacement, imageXsize, imageYsize);
        } else if (imageRotationStatus === 90) {
            this.viewCtx.rotate(90 * Math.PI/180);
            this.viewCtx.translate(0, -1*this.viewCanvas.clientWidth);
            this.viewCtx.drawImage(getRandomFrom(this.listOfImgs), imageYplacement, imageXplacement, imageYsize, imageXsize);
        } else if (imageRotationStatus === 180) {
            this.viewCtx.rotate(180 * Math.PI/180);
            this.viewCtx.translate(-1*this.viewCanvas.clientWidth, -1*this.viewCanvas.clientHeight);
            this.viewCtx.drawImage(getRandomFrom(this.listOfImgs), imageXplacement, imageYplacement, imageXsize, imageYsize);
        } else if (imageRotationStatus === 270) {
            this.viewCtx.rotate(270 * Math.PI/180);
            this.viewCtx.translate(-1*this.viewCanvas.clientHeight, 0);
            this.viewCtx.drawImage(getRandomFrom(this.listOfImgs), imageYplacement, imageXplacement, imageYsize, imageXsize);
        }

        this.viewCtx.restore();

    },
    downloadImage: function() {
        'use strict';

        let d = new Date();
        let timeStamp = (d.getFullYear() + (`00` + (d.getMonth() + 1)).slice(-2) + (`00` + d.getDate()).slice(-2) + `-` + (`00` + d.getHours()).slice(-2) + (`00` + d.getMinutes()).slice(-2) + `-` + (`00` + d.getSeconds()).slice(-2));

        this.downloadImageButton = document.createElement(`a`);
        this.downloadImageButton.download = `18-NPDU1-le_DWNLD_${timeStamp}.png`;

        this.downloadCtx.fillStyle = `#ffffeb`;

        this.downloadCtx.fillRect(0, 0, this.downloadCanvas.clientWidth, this.downloadCanvas.clientHeight);
        this.downloadCtx.drawImage(this.viewCanvas, 0, 0, this.downloadCanvas.clientWidth, this.downloadCanvas.clientHeight);

        this.downloadCanvas.toBlob((blob) => {
            this.downloadImageButton.href = URL.createObjectURL(blob);
            this.downloadImageButton.click();
            setTimeout(() => {
                URL.revokeObjectURL(this.downloadImageButton.href);
                this.downloadImageButton.href = null;
            }, 500);
        });
    },
    manageVariables: function() {
        'use strict';
        listOfRunStates.length == 1 ?
            this.currentState = listOfRunStates[0] :
            (() => {
                let previousState = this.currentState;
                while (previousState === this.currentState) {
                    this.currentState = getRandomFrom(listOfRunStates);// getRandomIntInclusive(0,12);
                }
            })();

        if (devMode === true) {console.log(this.currentState);}

        // this.frameLength = getRandomFrom([this.frameLength,]);//([50, 100, 200, 250, 350, 500, 750, 1000]);

        if (this.currentState == 0) {
            this.clearFirst = true;
            this.frameLength = getRandomFrom([this.frameLengthTarget, this.frameLengthInit]);
            this.layerPercentages = [100, 50, 25];
            this.blendModeListSelections = [1, 4, 3];
            this.currentFillColor = `transparent`;
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = false;
            this.manageDelayRange = [6000, 12000];
        } else if (this.currentState == 1) {
            this.clearFirst = false;
            this.frameLength = this.frameLengthTarget*4;
            this.layerPercentages = [100, 25, 20];
            this.blendModeListSelections = [1, 4, 3];
            this.currentFillColor = `random`;
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg) brightness(75%) contrast(200%)`;
            this.topBlur = false;
            this.manageDelayRange = [6000, 12000];
        } else if (this.currentState == 2) {
            this.clearFirst = false;
            this.frameLength = this.frameLengthTarget;
            this.layerPercentages = [100, 25, 20];
            this.blendModeListSelections = [1, 4, 3];
            this.currentFillColor = `random`;
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg) brightness(75%) contrast(200%)`;
            this.topBlur = false;
            this.manageDelayRange = [4000, 8000];
        } else if (this.currentState == 3) {
            this.clearFirst = false;
            this.frameLength = this.frameLengthTarget*5;
            this.layerPercentages = [100, 25, 20];
            this.blendModeListSelections = [1, 4, 3];
            this.currentFillColor = `random`;
            this.currentFilter = `grayscale(100%) brightness(75%) contrast(200%)`;
            this.topBlur = false;
            this.manageDelayRange = [4000, 8000];
        } else if (this.currentState == 4) {
            this.clearFirst = false;
            this.frameLength = this.frameLengthTarget;
            this.layerPercentages = [100, 25, 20];
            this.blendModeListSelections = [2, 4, 1];
            this.currentFillColor = getRandomFrom([`rgb(0, 0, 0)`, `rgb(48, 48, 48)`, `rgb(64, 0, 64)`,`rgb(0, 64, 64)`, `rgb(0, 0, 64)`]);
            this.currentFilter = `grayscale(100%) brightness(150%) contrast(250%)`;
            this.topBlur = false;
            this.manageDelayRange = [8000, 12000];
        } else if (this.currentState == 5) {
            this.clearFirst = true;
            this.frameLength = getRandomFrom([this.frameLengthTarget*2, this.frameLengthTarget*4]);
            this.layerPercentages = [100, 20, 10];
            this.blendModeListSelections = [2, 2, 2];
            this.currentFillColor = `transparent`;
            this.currentFilter = `saturate(${getRandomIntInclusive(15, 30)}%) contrast(${getRandomIntInclusive(150, 225)}%)`;
            this.topBlur = false;
            this.manageDelayRange = [6000, 12000];
        } else if (this.currentState == 6) {
            this.clearFirst = true;
            this.frameLength = this.frameLengthTarget*6;
            this.layerPercentages = [100, 50, 25];
            this.blendModeListSelections = [1, 1, 1];
            this.currentFillColor = `transparent`;
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = true;
            this.manageDelayRange = [4000, 8000];
        } else if (this.currentState == 7) {
            this.clearFirst = false;
            this.frameLength = this.frameLengthTarget*3;
            this.layerPercentages = [100, 0, 0];
            this.blendModeListSelections = [5, 5, 5];
            this.currentFillColor = `transparent`;
            this.currentFilter = `invert(100%)`;
            this.topBlur = false;
            this.manageDelayRange = [1500, 3000];
        } else if (this.currentState == 8) {
            this.clearFirst = true;
            this.frameLength = getRandomFrom([this.frameLengthTarget, this.frameLengthTarget*2,]);
            this.layerPercentages = [100, 75, 50];
            this.blendModeListSelections = [1, 6, 6];
            this.currentFillColor = `transparent`;//getRandomColor(`rgba`);
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = false;
            this.manageDelayRange = [2500, 5000];
        } else if (this.currentState == 9) {
            this.clearFirst = true;
            this.frameLength = getRandomFrom([this.frameLengthTarget*2, this.frameLengthTarget*3,]);
            this.layerPercentages = [100, 75, 50];
            this.blendModeListSelections = [7, 7, 7];
            this.currentFillColor = getRandomColor(`rgba`);
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = false;
            this.manageDelayRange = [3500, 7500];
        } else if (this.currentState == 10) {
            this.clearFirst = false;
            this.frameLength = getRandomFrom([Math.floor(this.frameLengthTarget*1.5), Math.floor(this.frameLengthTarget*2.5),]);
            this.layerPercentages = [100, 75, 50];
            this.blendModeListSelections = [7, 7, 7];
            this.currentFillColor = getRandomColor(`rgba`);
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = false;
            this.manageDelayRange = [2500, 5000];
        } else if (this.currentState == 11) {
            this.clearFirst = false;
            this.frameLength = getRandomFrom([1000, 1500, 2000, 2500, 3000]);
            this.layerPercentages = [100, 75, 50];
            this.blendModeListSelections = [1, 1, 1];
            this.currentFillColor = `transparent`;
            this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
            this.topBlur = false;
            this.manageDelayRange = [3000, 7500];
        } else if (this.currentState == 12) {
            this.clearFirst = false;
            this.frameLength = getRandomFrom([1000, 1250, 1500, 1750, 2000]);
            this.layerPercentages = [100, 75, 50];
            this.blendModeListSelections = [1, 1, 1];
            this.currentFillColor = `transparent`;
            this.currentFilter = `grayscale(100%) brightness(150%) contrast(250%)`;
            this.topBlur = false;
            this.manageDelayRange = [3000, 7500];
        }

        // this.clearFirst === false ? (() => {
        //     this.clearFirst = true;
        //     this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg)`;
        //     this.manageDelayRange = [6000, 12000];
        // })() : (() => {
        //     this.clearFirst = false;
        //     this.currentFilter = `hue-rotate(${getRandomIntInclusive(0,359)}deg) brightness(75%) contrast(200%)`;
        //     this.manageDelayRange = [3000, 7000];
        // })();

        setTimeout(() => {
            this.manageVariables.call(this);
        }, getRandomIntInclusive(this.manageDelayRange[0], this.manageDelayRange[1]));
    }
};

export default Thing;

// module.exports = {
//     Thing: Thing
// };

// console.log(module.exports.Thing);


//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//
//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//
