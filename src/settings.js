let currentURL = String(window.location.href);
// const queryString = currentURL.split(`?`)[1];

// let devMode = false;
// if (queryString && queryString.includes(`dev`)) {
//     devMode = true;
// }

// let assetPath = `../18-NPDU1-le_assets/`;
// if (queryString && queryString.includes(`hiRes`)) {
//     assetPath = `../18-NPDU1-le_assets-hiRes/`;
// }

currentURL = new URL(window.location);
const qArgs = new URLSearchParams(currentURL.search);

let devMode;
qArgs.get(`devMode`) != null && qArgs.get(`devMode`) !=`false` ? devMode = true : devMode = false;

let assetPath;
let assetSuffix = ``;
qArgs.get(`hiRes`) != null && qArgs.get(`hiRes`) !=`false` ?
    assetPath = `../18-NPDU1-le_assets-hiRes/` :
    qArgs.get(`midRes`) != null && qArgs.get(`midRes`) !=`false` ?
        (() => {
            assetPath = `../18-NPDU1-le_assets/`;
            assetSuffix = `_midRes`;
        })() :
        (() => {
            assetPath = `../18-NPDU1-le_assets-loRes/`;
            assetSuffix = `_loRes`;
        })();

let listOfRunStates;
qArgs.get(`runStates`) != null && qArgs.get(`runStates`) !=`false` ? listOfRunStates = qArgs.get(`runStates`).split(`,`) : listOfRunStates = [0,1,2,3,4,5,6,7,8,9,10,11,12];

if (devMode === true) {console.log(`
use query string 'hiRes' or 'midRes' for higher resolution images ...

use query string 'runStates=*,*,*' to specify a limited selection of runstate for the app to choose from ...

`);}

if (devMode === true) {console.log(`listOfRunStaes: ${listOfRunStates}`);}

if (devMode === true) {console.log(`devMode: ${devMode}`);}
if (devMode === true) {console.log(`assetPath: ${assetPath}`);}

let platformTest;
if (
    (
        (navigator.platform.indexOf(`MacIntel`) >= 0) ||
        (navigator.platform.indexOf(`Win64`) >= 0) ||
        (navigator.userAgent.indexOf(`Win64`) >= 0)
    ) && (
        (navigator.userAgent.indexOf(`Chrome`) >= 0) ||
        (navigator.userAgent.indexOf(`Firefox`) >= 0)
    )
) {
    platformTest = true;
} else {
    platformTest = false;
}

const listOfImages = [
    `18-NPDU1-tn-002_201811122337_v02`,
    `18-NPDU1-tn-002_201811122337`,
    `18-NPDU1-tn-003_201811131211_v02`,
    `18-NPDU1-tn-003_201811131211`,
    `18-NPDU1-tn-003_201811131238_v02`,
    `18-NPDU1-tn-003_201811131238`,
    `18-NPDU1-tn-003_201811131601_v02`,
    `18-NPDU1-tn-003_201811131601`,
    `18-NPDU1-tn-004_201811131613_v02`,
    `18-NPDU1-tn-004_201811131613_v03`,
    `18-NPDU1-tn-004_201811131613`,
    `18-NPDU1-tn-004_201811140017_v02`,
    `18-NPDU1-tn-004_201811140017_V03`,
    `18-NPDU1-tn-004_201811140017`,
    `18-NPDU1-tn-005_201811140102_v02`,
    `18-NPDU1-tn-005_201811140102`,
    `18-NPDU1-tn-005_201811140231_v02`,
    `18-NPDU1-tn-005_201811140231`,
    `18-NPDU1-tn-006_201811241726_v02`,
    `18-NPDU1-tn-006_201811241726`,
    `18-NPDU1-tn-007_201811252359`,
    `18-NPDU1-tn-007_201811252359_v02`,
    `18-NPDU1-tn-007_201811252359_v03`,
    `18-NPDU1-tn-008_201811261043`,
    `18-NPDU1-tn-008_201811261043_v02`,
    `18-NPDU1-tn-008_201811261109`,
    `18-NPDU1-tn-008_201811261108`,
];

const listOfBlendModesALL = [`source-over`, `source-in`, `source-out`, `source-atop`, `destination-over`, `destination-in`, `destination-atop`, `lighter`, `copy`, `xor`, `multiply`, `screen`, `overlay`, `darken`, `lighten`, `color-dodge`, `color-burn`, `hard-light`, `soft-light`, `difference`, `exclusion`, `hue`, `saturation`, `color`, `luminosity`];
const listOfBlendModes = [`lighter`, `multiply`, `screen`, `overlay`, `darken`, `lighten`, `color-dodge`, `color-burn`, `hard-light`, `soft-light`, `difference`, `exclusion`, `hue`, `saturation`, `color`, `luminosity`];
const listOfBlendModesLIGHT = [`lighter`, `screen`, `lighten`, `color-dodge`];
const listOfBlendModesDARK = [`multiply`, `darken`, `color-burn`];
const listOfBlendModesINVERT = [`difference`, `exclusion`];
const listOfBlendModesCUSTOMa = [`overlay`,];
const listOfBlendModesCUSTOMb = [`source-out`, `destination-out`];
const listOfBlendModesCUSTOMc = [`soft-light`, `overlay`];


const listOfBlemdModeLists = [
    listOfBlendModesALL, // 0
    listOfBlendModes, // 1
    listOfBlendModesLIGHT, // 2
    listOfBlendModesDARK, // 3
    listOfBlendModesINVERT, // 4
    listOfBlendModesCUSTOMa, // 5
    listOfBlendModesCUSTOMb, // 6
    listOfBlendModesCUSTOMc, // 7
];

export { devMode, assetPath, assetSuffix, listOfRunStates, platformTest, listOfImages, listOfBlemdModeLists };