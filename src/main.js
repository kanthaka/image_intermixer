import './main.css';
import './loadingSpinner.css';
import { devMode } from './settings.js';
import Thing from './Thing.js'; // let Thing = require('./Thing.js');

// if (devMode === true) {console.log(`hello world`);}
// if (devMode === true) {console.log(`devMode: ${devMode}`);}

window.thing00 = new Thing(`00`);
// if (devMode === true) {thing00.sayHello();}

window.loadTimerStart = new Date();

window.resizeCanvas = function() {
    'use strict';
    if (window.thing00.viewCanvas != undefined) {
        window.thing00.viewCanvas.width = Math.floor(window.thing00.thingView.clientWidth+10);
        window.thing00.viewCanvas.height = Math.floor(window.thing00.thingView.clientWidth
            +10);
        window.thing00.drawCanvas(`onePlay`);
        if (devMode != false) {
            window.thing00.downloadCanvas.width = Math.floor(window.thing00.thingView.clientWidth+10);
            window.thing00.downloadCanvas.height = Math.floor(window.thing00.thingView.clientWidth
                +10);
        }
    }
};

let resizeTimeout = null;
window.addEventListener(`resize`, () => {
    'use strict';
    window.resizeCanvas(1);
    if (!resizeTimeout) {
        resizeTimeout = setTimeout(function () {
            resizeTimeout = null;
            window.resizeCanvas();
        }, 250);  // 1000 / 250 = 4fps
    }
}, false);
