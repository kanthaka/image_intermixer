function getRandomIntInclusive(a, b) {
    'use strict';
    var min;
    var max;
    if (a <= b) {
        min = Math.ceil(a);
        max = Math.floor(b);
    } else {
        min = Math.ceil(b);
        max = Math.floor(a);
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomFrom(list) {
    "use strict";
    var itemIndex = getRandomIntInclusive(0, list.length-1);
    return list[itemIndex];
}

function getRandomColor(colorFormat) {
    // if colorFormat is set to 'rgbaRandomAlpha', the alpha channel will be randomized
    // if colorFormat is set to 'rgbaFullAlpha'', the alpha channel will be set to '1.0'
    // if colorFormat is set to 'rgb', the function returns an RGB value
    // if colorFormat is set to 'hex', the function ruturns a hex color value
    // if no colorFormat value is given, the aplhaSwitch will default to true
    'use strict';
    if (colorFormat === undefined) {
        colorFormat = `rgba`;
    }
    var colorList = [];
    var i;
    for (let i = 0; i < 3; i++) {
        colorList.push(getRandomIntInclusive(0, 255));
    }
    if (colorFormat === `rgbaRandomAlpha` || colorFormat === `rgba`) {
        colorList.push(getRandomIntInclusive(0, 100) / 100);
        return `rgba(` + colorList.join(`, `) + `)`;
    }
    if (colorFormat === `rgbaFullAlpha`) {
        colorList.push(`1.0`);
        return `rgba(` + colorList.join(`, `) + `)`;
    }
    if (colorFormat === `rgb`) {
        return `rgb(` + colorList.join(`, `) + `)`;
    }
    if (colorFormat === `hex`) {
        colorList = [];
        var hexList = [`0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `a`, `b`, `c`, `d`, `e`, `f`];
        for (let i = 0; i < 6; i++) {
            colorList.push(hexList[getRandomIntInclusive(0, 15)]);
        }
        return `#` + colorList.join(``);
    }
    return undefined;
}

function removeAllInstancesOfItemFromList(item, list) {
    "use strict";
    while (list.indexOf(item) >= 0) {
        list.splice(list.indexOf(item), 1);
    }
}

export { getRandomIntInclusive, getRandomFrom, getRandomColor, removeAllInstancesOfItemFromList };