# IMAGE_INTERMIXER

This is an experimental tool for converting pools of still images into micro-cut video sequences.  It runs the images through a series of "run states", each of which has its own semi-random methods for rotating, re-sizing, overlaying, blending, and applying effects to the images.

The presentation can run at up to 20fps - please be aware that this can result in fairly **intense flashing lights**, which may present a hazard for some users.

Clicking on the display will cause the app to pause the presentation ... a button in the upper right corner of the screen will allow the user to download the current still image.

[A live demo of this app should be available here.](https://concretesunrise.net/18/NPDU1/le/)

Browser sniffing features will fire if the app is opened with Safari, but technically it should still work in Safari.

Mobile devices are not supported ... tests carried out on an iPhone Xr show that the app struggles to run smoothly on that device.

---

QUERY STRINGS:

- `devMode` will allow console logs to show in the dev tools console ... this will allow the user to see which sequence is currently running at any given time

- `hiRes` and `mid-res` will force the app to load high- or medium-resolution assets (this will result in more stuttering on less powerful devices)

- `runStates=<number>,<number>,...` will force the app to only use a specific list of the available run state sequences.  Currently there are 13 total run states, numbered 0-12.  Running the app in devMode will allow the user to identify which run states they want to list here.  Regardless of the runState setting, the app will always start up by running sequence 0 for a few seconds.

---

NOTES:

- this project was previously listed under the code-name "18-NPDU1-le"